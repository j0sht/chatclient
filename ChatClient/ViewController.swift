//
//  ViewController.swift
//  ChatClient
//
//  Created by Joshua Tate on 2020-03-27.
//  Copyright © 2020 icebreak. All rights reserved.
//

import UIKit

struct Message: Codable {
    let message: String
}

class ViewController: UIViewController, UITableViewDataSource, UITableViewDelegate  {

    private let cellID = "chatCell"
    private let session = URLSession(configuration: .default)
    private var webSocketTask: URLSessionWebSocketTask!

    private var messages = [Message]()

    @IBOutlet weak var textField: UITextField!
    @IBOutlet weak var tableView: UITableView!

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        webSocketTask = session.webSocketTask(with: URL(string: "ws://localhost:8000/ws/chat/main/")!)
        webSocketTask.resume()
        receiveMessage()
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return messages.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellID, for: indexPath)
        cell.textLabel?.text = messages[indexPath.row].message
        return cell
    }

    @IBAction func sendButtonPressed(_ sender: UIButton) {
        if let text = textField.text {
            sendMessage(text)
        }
        textField.text = ""
    }

    private func receiveMessage() {
        webSocketTask.receive { [unowned self] (result) in
            switch result {
            case .success(let msg):
                switch msg {
                case .string(let s):
                    if let data = s.data(using: .utf8), let message = try? JSONDecoder().decode(Message.self, from: data) {
                        self.messages.append(message)
                        DispatchQueue.main.async {
                            self.tableView.reloadData()
                        }
                    }
                case .data(let d):
                    print("Received data: \(d)")
                @unknown default:
                    fatalError()
                }
                self.receiveMessage()
            case .failure(let e):
                print("Error occured receiving message: \(e)")
            }
        }
    }

    private func sendMessage(_ s: String) {
        let msg = Message(message: s)
        let json = try! JSONEncoder().encode(msg)
        let jsonStr = String(data: json, encoding: .utf8)!
        let message = URLSessionWebSocketTask.Message.string(jsonStr)
        webSocketTask.send(message) { (error) in
            if let error = error {
                print("Error sending message! \(error)")
            }
        }
    }
}

